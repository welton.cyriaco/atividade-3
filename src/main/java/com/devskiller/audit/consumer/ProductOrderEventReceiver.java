package com.devskiller.audit.consumer;

import java.io.IOException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;

import com.devskiller.audit.service.ProductOrderEventProcessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class ProductOrderEventReceiver {

	private ObjectMapper mapper = new ObjectMapper();

    public ProductOrderEventReceiver() {
        mapper.registerModule(new JavaTimeModule());
    }

	@Autowired
    private ProductOrderEventProcessor processor;

	@KafkaListener(topics = "product", groupId = "audit")
    public void receive(ConsumerRecord<?, ?> consumerRecord) throws IOException {
		ProductOrderEvent event = mapper.readValue(consumerRecord.value().toString(), ProductOrderEvent.class);
        processor.processEvent(event);
    }

}
