package com.devskiller.audit.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.devskiller.audit.consumer.ProductOrderEvent;
import com.devskiller.audit.model.ProductOrder;
import com.devskiller.audit.repository.ProductOrderRepository;

public class ProductOrderEventProcessor {

	@Autowired
    private ProductOrderRepository productOrderRepository;
	
    public void processEvent(ProductOrderEvent event) {
    	ProductOrder productOrder = new ProductOrder(event.getUserLogin(), event.getProductId(), event.getCreationTime());
    	productOrderRepository.save(productOrder);
    }

}
